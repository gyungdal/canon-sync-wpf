﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using EOSDigital.API;
using EOSDigital.SDK;

namespace CanonSyncWPF {
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window {
        #region Variables

        private CanonAPI APIHandler;
        private CameraValue[] AvList;
        private CameraValue[] TvList;
        private CameraValue[] ISOList;
        private List<bool> alreadyDownload;
        private List<Camera> CamList;
        private const int BulbTime = 30;
        private System.Windows.Forms.FolderBrowserDialog SaveFolderBrowser = new System.Windows.Forms.FolderBrowserDialog ();

        private int ErrCount;
        private object ErrLock = new object ();

        #endregion

        public MainWindow () {
            try {
                InitializeComponent ();
                alreadyDownload = new List<bool>();
                APIHandler = new CanonAPI ();
                APIHandler.CameraAdded += APIHandler_CameraAdded;
                ErrorHandler.SevereErrorHappened += ErrorHandler_SevereErrorHappened;
                ErrorHandler.NonSevereErrorHappened += ErrorHandler_NonSevereErrorHappened;
                SavePathTextBox.Text =
                    System.IO.Path.Combine(
                        Environment.GetFolderPath(Environment.SpecialFolder.MyPictures),
                        DateTime.Now.ToString("yyyyMMdd_HHmm")
                    );
                SaveFolderBrowser.Description = "Save Images To...";
                RefreshCamera ();
            } catch (DllNotFoundException) {
                ReportError ("Canon DLLs not found!", true);
            } catch (Exception ex) {
                ReportError (ex.Message, true);
            }
        }

        private void Window_Closing (object sender, CancelEventArgs e) {
            try {
                APIHandler?.Dispose ();
            } catch (Exception ex) { ReportError (ex.Message, false); }
        }

        #region API Events

        private void APIHandler_CameraAdded (CanonAPI sender) {
            try {
                Dispatcher.Invoke ((Action) delegate {
                    RefreshCamera ();
                });
            } catch (Exception ex) {
                ReportError (ex.Message, false);
            }
        }

        private void Camera_StateChanged (Camera sender, StateEventID eventID, int parameter) {
            try {
                if (eventID == StateEventID.Shutdown) {
                    Dispatcher.Invoke ((Action) delegate { CloseSession (); });
                }
            } catch (Exception ex) {
                ReportError (ex.Message, false);
            }
        }

        private void Camera_DownloadReady (Camera sender, DownloadInfo Info) {
            try {
                alreadyDownload[CamList.FindIndex(camera => sender == camera)] = true;  
                string dir = null;
                SavePathTextBox.Dispatcher.Invoke ((Action) delegate { dir = SavePathTextBox.Text; });
                sender.DownloadFile (Info, dir);
                MainProgressBar.Dispatcher.Invoke ((Action) delegate {
                    //true인 것들만 분리
                    double pos = alreadyDownload.FindAll(value => value).Count;
                    MainProgressBar.Value = (pos / alreadyDownload.Count) * 100;
                });
            }
            catch (Exception ex) { ReportError (ex.Message, false); }
        }

        private void ErrorHandler_NonSevereErrorHappened (object sender, ErrorCode ex) {
            ReportError ($"SDK Error code: {ex} ({((int)ex).ToString("X")})", false);
        }

        private void ErrorHandler_SevereErrorHappened (object sender, Exception ex) {
            ReportError (ex.Message, true);
        }

        #endregion

        #region Session

        
        private void RefreshButton_Click (object sender, RoutedEventArgs e) {
            try {
                RefreshCamera ();
            } catch (Exception ex) {
                ReportError (ex.Message, false);
            }
        }

        #endregion

        #region Settings

        private void TakePhotoButton_Click (object sender, RoutedEventArgs e) {
            try {
                bool temp = (string)TvCoBox.SelectedItem == "Bulb";
                TakePhotoButton.IsEnabled = false;
                if(alreadyDownload.Count != 0)
                {
                    alreadyDownload.Clear();
                }

                CamList.ForEach(camera =>
                {
                    alreadyDownload.Add(false);
                    if (!camera.SessionOpen)
                        camera.OpenSession();
                        
                    camera.StateChanged += Camera_StateChanged;
                    camera.DownloadReady += Camera_DownloadReady;
                    camera.SetSetting(PropertyID.Av, AvValues.GetValue((string)AvCoBox.SelectedItem).IntValue);
                    camera.SetSetting(PropertyID.Tv, TvValues.GetValue((string)TvCoBox.SelectedItem).IntValue);
                    camera.SetSetting(PropertyID.ISO, ISOValues.GetValue((string)ISOCoBox.SelectedItem).IntValue);
                    camera.SetSetting(PropertyID.SaveTo, (int)SaveTo.Host);
                    camera.SetCapacity(4096, int.MaxValue);
                });

                Parallel.ForEach(CamList, camera =>
                {
                    if (temp)
                        camera.TakePhotoBulb(BulbTime);
                    else
                        camera.TakePhoto();
                });

                TakePhotoButton.IsEnabled = true;
                BrowseButton.IsEnabled = true;
                SavePathTextBox.IsEnabled = true;
                
            } catch (Exception ex) { ReportError (ex.Message, false); }
        }
        
        private void BrowseButton_Click (object sender, RoutedEventArgs e) {
            try {
                if (Directory.Exists (SavePathTextBox.Text)) SaveFolderBrowser.SelectedPath = SavePathTextBox.Text;
                if (SaveFolderBrowser.ShowDialog () == System.Windows.Forms.DialogResult.OK) {
                    SavePathTextBox.Text = SaveFolderBrowser.SelectedPath;
                }
            } catch (Exception ex) { ReportError (ex.Message, false); }
        }

        #endregion

        #region Subroutines

        private void CloseSession () {
            CamList.ForEach(camera =>
            {
                if (camera.SessionOpen)
                {
                    camera.CloseSession();
                }
            });
            AvCoBox.Items.Clear ();
            TvCoBox.Items.Clear ();
            ISOCoBox.Items.Clear ();
        }

        private void RefreshCamera () {
            CamList = APIHandler.GetCameraList ();
            CameraListBox.Items.Clear();
            CamList.ForEach(camera => CameraListBox.Items.Add(camera.DeviceName));
            if (CamList.Count > 0)
            {
                Camera camera = CamList[0];
                camera.OpenSession();
                
                AvList = camera.GetSettingsList(PropertyID.Av);
                TvList = camera.GetSettingsList(PropertyID.Tv);
                ISOList = camera.GetSettingsList(PropertyID.ISO);

                AvCoBox.Items.Clear();
                TvCoBox.Items.Clear();
                ISOCoBox.Items.Clear();

                Array.ForEach(AvList, av => AvCoBox.Items.Add(av.StringValue));
                Array.ForEach(TvList, tv => TvCoBox.Items.Add(tv.StringValue));
                Array.ForEach(ISOList, iso => ISOCoBox.Items.Add(iso.StringValue));

                AvCoBox.SelectedIndex = AvCoBox.Items.IndexOf(AvValues.GetValue(camera.GetInt32Setting(PropertyID.Av)).StringValue);
                TvCoBox.SelectedIndex = TvCoBox.Items.IndexOf(TvValues.GetValue(camera.GetInt32Setting(PropertyID.Tv)).StringValue);
                ISOCoBox.SelectedIndex = ISOCoBox.Items.IndexOf(ISOValues.GetValue(camera.GetInt32Setting(PropertyID.ISO)).StringValue);

                camera.CloseSession();
            }
        }


        private void ReportError (string message, bool lockdown) {
            int errc;
            lock (ErrLock) { errc = ++ErrCount; }

            if (lockdown) EnableUI (false);

            if (errc < 4) MessageBox.Show (message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            else if (errc == 4) MessageBox.Show ("Many errors happened!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);

            lock (ErrLock) {
                ErrCount--;
            }
        }

        private void EnableUI (bool enable) {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke((Action)delegate
                {
                    EnableUI(enable);
                });
            }
        }

        #endregion
    }
}